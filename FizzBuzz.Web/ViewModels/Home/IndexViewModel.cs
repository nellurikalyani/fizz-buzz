﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Web.ViewModels.Home
{
    public class IndexViewModel
    {
        [Required,Range(1,1000)]
        public int Number { get; set; }

        public IPagedList<String> Result { get; set; }
    }
}