﻿using FizzBuzz.Web.ViewModels.Home;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Core.Services;
using PagedList;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzBuzzService _fizzBuzzService;

        public HomeController()
        {
            _fizzBuzzService = new FizzBuzzService();
        }

        public HomeController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
        }

        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(IndexViewModel viewModel, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);
            if (!ModelState.IsValid)
            {
                viewModel.Result = null;
                return View(viewModel);
            };
            
            
            viewModel.Result = _fizzBuzzService.FizzBuzz(viewModel.Number).ToPagedList(pageNumber, pageSize);

            return RedirectToAction("FizzBuzz", new { value = viewModel.Number, page = 1 });
        }

        [HttpGet]
        public ActionResult FizzBuzz(int value, int? page)
        {
            int pageSize = 20;
            int pageNumber = (page ?? 1);

            var model = new IndexViewModel();
            model.Number = value;

            model.Result = _fizzBuzzService.FizzBuzz(model.Number).ToPagedList(pageNumber, pageSize);

            return View(model);


        }
    }
}