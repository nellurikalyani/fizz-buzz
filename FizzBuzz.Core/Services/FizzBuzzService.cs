﻿using System;
using System.Collections.Generic;

namespace FizzBuzz.Core.Services
{
    public class FizzBuzzService : IFizzBuzzService
    {
        public List<string> FizzBuzz(int number)
        {
            if (number < 1 || number > 1000)
            {
                throw new ArgumentOutOfRangeException(string.Format("Value must be between 1 and 1000 but was {0}", number));
            }
            List<string> result = new List<string>();
            for (int i = 1; i <= number; i++)
            {
                string resultValue;
                if (i % 15 == 0)
                    resultValue = GetDivisibleByThreeAndFiveValue(i);
                else if (i % 3 == 0)
                    resultValue = GetDivisibleByThreeValue(i);
                else if (i % 5 == 0)
                    resultValue = GetDivisibleByFiveValue(i);
                else
                    resultValue = i.ToString();
                result.Add(resultValue);
            }
            return result;
        }

        public  String GetDivisibleByThreeAndFiveValue(int i)
        {                        
                return "FizzBuzz";                        
        }
        public  String GetDivisibleByThreeValue(int i)
        {
            DateTime dt = new DateTime();
            if (dt.DayOfWeek == DayOfWeek.Wednesday)
                return "Wizz";
            return "Fizz";
        }
        public  String GetDivisibleByFiveValue(int i)
        {
            DateTime dt = new DateTime();
            if (dt.DayOfWeek == DayOfWeek.Wednesday)
                return "Wuzz";
            return "Buzz";
        }
    }
}
