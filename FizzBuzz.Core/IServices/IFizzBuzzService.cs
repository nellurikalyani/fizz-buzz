﻿using System;
using System.Collections.Generic;

namespace FizzBuzz.Core.Services
{
    public interface IFizzBuzzService
    {
        List<string> FizzBuzz(int number);
        String GetDivisibleByThreeAndFiveValue(int i);
        String GetDivisibleByThreeValue(int i);
        String GetDivisibleByFiveValue(int i);
    }
}
