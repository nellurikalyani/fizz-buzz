﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using FizzBuzz.Core.Services;

namespace FizzBuzz.Core.Tests.Services
{
    [TestFixture]
    public class FizzBuzzServiceTests
    {
        private FizzBuzzService GetService()
        {
            return new FizzBuzzService();
        }

        [Test]
        public void CanCreateService()
        {
            Assert.IsNotNull(GetService());
        }

        [Test]
        public void CheckReturnsTheNumberForNonMulipleOf3Or5()
        {
            Assert.AreEqual("7", GetService().FizzBuzz(7));
        }

        [Test]
        public void CheckReturnsFizzForMultipleOf3()
        {
            Assert.AreEqual("Fizz", GetService().FizzBuzz(6));
        }

        [Test]
        public void CheckReturnsBuzzForMultipleOf5()
        {
            Assert.AreEqual("Buzz", GetService().FizzBuzz(10));
        }

        [Test]
        public void CheckReturnsFizzBuzzForMultipleOf3And5()
        {
            Assert.AreEqual("FizzBuzz", GetService().FizzBuzz(30));
        }

        [Test]
        public void CheckReturnsWizzWuzzForWednesday()
        {
            Assert.AreEqual("WizzWuzz", GetService().FizzBuzz(4));
        }

        [Test]
        public void CheckReturns1For1()
        {
            Assert.AreEqual("1", GetService().FizzBuzz(1));
        }

        [Test]
        public void CheckReturns2For2()
        {
            Assert.AreEqual("2", GetService().FizzBuzz(2));
        }

        [Test]
        public void CheckReturnsFizzFor3()
        {
            Assert.AreEqual("Fizz", GetService().FizzBuzz(3));
        }

        [Test]
        public void CheckReturnsBuzzFor5()
        {
            Assert.AreEqual("Buzz", GetService().FizzBuzz(5));
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CheckThrowsArgumentOfRangeExceptionFor0()
        {
            var result = GetService().FizzBuzz(0);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CheckThrowsArgumentOfRangeExceptionForValueLessThan1()
        {
            var result = GetService().FizzBuzz(-1);
        }

        [Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void CheckThrowsArgumentOfRangeExceptionForValueGreaterThan1000()
        {
            var result = GetService().FizzBuzz(1001);
        }
    }
}
