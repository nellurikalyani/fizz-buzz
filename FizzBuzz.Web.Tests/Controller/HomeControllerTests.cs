﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using NUnit.Framework;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Core.Services;

namespace FizzBuzz.Web.Tests.Controller
{
    [TestFixture]
    public class HomeControllerTests
    {
        private class DummyFizzBuzzService : IFizzBuzzService
        {
            public List<string> FizzBuzz(int number)
            {
                List<string> res = new List<string>();
                res.Add("FizzBuzzDummyResult");
                return res;
            }
            public String GetDivisibleByThreeAndFiveValue(int i)
            {
                return "FizzBuzz";
            }
            public String GetDivisibleByThreeValue(int i)
            {
                DateTime dt = new DateTime();
                if (dt.DayOfWeek == DayOfWeek.Wednesday)
                    return "Wizz";
                return "Fizz";
            }
            public String GetDivisibleByFiveValue(int i)
            {
                DateTime dt = new DateTime();
                if (dt.DayOfWeek == DayOfWeek.Wednesday)
                    return "Wuzz";
                return "Buzz";
            }
        }

        [Test]
        public void CanCreate()
        {
            var controller = new HomeController();
            Assert.IsNotNull(controller);
        }

        [Test]
        public void CanCreateWithFizzBuzzService()
        {
            var service = new DummyFizzBuzzService();
            var controller = new HomeController(service);
            Assert.IsNotNull(controller);
        }

        [Test]
        public void CheckReturnsCorrectView()
        {
            var service = new DummyFizzBuzzService();
            var controller = new HomeController(service);

            var result = (ViewResult)controller.Index();

            Assert.AreEqual("Index", result.ViewName);
        }

       
    }
}
